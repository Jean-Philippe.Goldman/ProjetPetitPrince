from flask import render_template, url_for
from prince import app, db
from prince.models import Language, Sound


@app.route('/')
@app.route('/home')
def home():
    prince_language = Language.query.filter_by(actif=1)
    sounds = Sound.query.all()
    return render_template('home.html', title='Petit Prince', prince_language = prince_language, sounds = sounds)

@app.route('/liste_langues')
def liste_langues():
    prince_language = Language.query.filter_by(actif=1)
    sounds = Sound.query.all()
    return render_template('liste_langues.html', title='Liste des langues', prince_language = prince_language, sounds = sounds)

@app.route('/langue/<lang>')
def langue(lang):
    langue_complet = Language.query.filter_by(french = lang).first()
    sounds = Sound.query.filter_by(language = langue_complet.iso)
    return render_template('langue.html', langue=langue_complet, title=lang, sounds = sounds)

@app.route('/carte')
def carte():
    french = Language.french
    prince_language = Language.query.filter_by(actif=1)
    sounds = Sound.query.all()
    return render_template('carte.html', title='Carte', prince_language=prince_language, sounds = sounds)

@app.route('/enregistrement')
def enregistrement():
    return render_template('enregistrement.html', title='Enregistrer un audio')

@app.route('/about')
def about():
    prince_language = Language.query.filter_by(actif=1)
    return render_template('about.html', title='À propos', prince_language = prince_language)
